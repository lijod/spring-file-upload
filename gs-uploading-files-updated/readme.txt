To run this project, you will need to update persistence.xml
Attributes to be updated:
javax.persistence.jdbc.password
javax.persistence.jdbc.user
javax.persistence.jdbc.url

Command to run:
cd path/to/project
gradlew build
java -jar build\libs\gs-uploading-files-0.1.0.jar