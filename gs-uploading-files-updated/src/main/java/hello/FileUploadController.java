package hello;

import hello.constant.FileConstant;
import hello.util.FileUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.CloseShieldInputStream;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * This controller handles the /upload GET & POST request.
 * 
 * @author Lijo Daniel
 */
@Controller
public class FileUploadController { // NOPMD by Lijo Daniel on 16/1/15 4:33 AM

	/**
	 * maxFileSize from application.properties
	 */
	@Value("${maxFileSize}")
	private transient long maxFileSize;
	
	/**
	 * This method handles the /upload GET request. This will respond with a
	 * message saying only POST is supported.
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	public final String provideUploadInfo() {
		return FileConstant.MSG_POST_SUPPORT;
	}

	/**
	 * This method handles the /upload POST request. The uploaded file will be
	 * saved on the file system and also persisted in database.
	 * 
	 * @param file
	 * @param response
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST, 
			produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public final void handleFileUpload(
			@RequestParam("file") final MultipartFile file,
			final HttpServletResponse response) {
		if (!file.isEmpty()) {
			InputStream inputStream = null; //NOPMD
			try {
				if(file.getSize() > maxFileSize){
					throw new FileUploadException("File size exceeded, "
							+ "max upload limit:" + maxFileSize);
				}
				inputStream = file.getInputStream();
				FileUtil.saveFileInFileSystem(new CloseShieldInputStream(inputStream),
						file.getOriginalFilename());
				FileUtil.saveFileInDatabase(new CloseShieldInputStream(inputStream),
						file.getOriginalFilename());
				response.setHeader(
						FileConstant.HTTPREQ_CONTENT_DISPOSITION,
						FileConstant.HTTPREQ_CONTENT_DISPOSITION_VAL
								+ file.getOriginalFilename());
				IOUtils.copy(
						new FileInputStream(
								new File(file.getOriginalFilename())), response
								.getOutputStream());
				response.flushBuffer();
			} catch (FileUploadException e) {
				System.out.println(e.getMessage()); // NOPMD
			} catch (IOException e) {
				System.out.println(e.getMessage()); // NOPMD
			} catch (SQLException e) {  
				System.out.println(e.getMessage()); // NOPMD
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		}
	}

}
