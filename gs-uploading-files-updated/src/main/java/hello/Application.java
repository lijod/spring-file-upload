package hello;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration

/**
 * This class is used to configure the server application to handle requests.
 * @author Lijo Daniel
 */
public class Application { // NOPMD by Lijo Daniel on 16/1/15 4:23 AM

	/**
	 * main() acts as the entry point to the application. Upon being called, the
	 * server application will be configured.
	 * 
	 * @param args
	 */
	public static void main(final String... args) {
		SpringApplication.run(Application.class, args);
	}

	/**
	 * Configures the max upload size for the application 
	 * @return
	 */
	@Bean
    public MultipartConfigElement multipartConfigElement() {
        final MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("-1");
	    factory.setMaxRequestSize("-1");
	    return factory.createMultipartConfig();
	}
	
}
