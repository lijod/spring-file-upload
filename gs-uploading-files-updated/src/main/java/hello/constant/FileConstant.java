package hello.constant;

/**
 * This class maintains the global constants used in the application.
 * @author Lijo Daniel
 */
public final class FileConstant {
	
	/**
	 * Specifies the name of the JPA persistence unit.
	 */
	public final static String PERSISTENCE_UNIT_NAME = "upload";  // NOPMD 
	
	/**
	 * Specifies the HTTP header property Content-Disposition.
	 */
	public final static String HTTPREQ_CONTENT_DISPOSITION =  // NOPMD 
			"Content-Disposition"; 					
	
	/**
	 * Specifies the HTTP header value for Content-Disposition.
	 */
	public final static String HTTPREQ_CONTENT_DISPOSITION_VAL =  // NOPMD 
			"attachment; filename="; 				
	
	/**
	 * 
	 */
	public final static String MSG_POST_SUPPORT = 
			"You can upload a file by posting to this same URL.";
	
	private FileConstant() { }
	
}
