package hello.util;

import hello.dao.FileDao;
import hello.dao.impl.FileDaoImpl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import org.apache.commons.io.IOUtils;

/**
 * This class contains the utility methods for this application.
 * 
 * @author Lijo Daniel
 */
public final class FileUtil {

	private FileUtil() {
	}

	/**
	 * Saves the given input stream in the file system with the given file name,
	 * the returned boolean indicates success where true means success.
	 * 
	 * @param fileBytes
	 * @param fileName
	 * @return
	 */
	@SuppressWarnings({"PMD.LawOfDemeter", "PMD.DataflowAnomalyAnalysis"})
	public static boolean saveFileInFileSystem(final InputStream stream,
			final String fileName) {
		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(fileName);
			final byte[] bytes = new byte[2048];
			int read = 0;
			while((read = stream.read(bytes)) != -1){ //NOPMD
				outputStream.write(bytes, 0 , read);
			}
			return true; // NOPMD
		} catch (IOException e) {
			System.out.println(e.getMessage()); // NOPMD
			return false;
		} finally {
			IOUtils.closeQuietly(stream);
			IOUtils.closeQuietly(outputStream);
		}
	}

	/**
	 * Saves the byte stream to the database with corresponding file name.
	 * 
	 * @param fileBytes
	 * @param fileName
	 * @throws SQLException 
	 */
	public static void saveFileInDatabase(final InputStream filestream,
			final String fileName) throws SQLException {
		final FileDao fileDao = FileDaoImpl.getInstance();
		fileDao.persistFile(fileName, filestream); // NOPMD by Lijo Daniel on
													// 16/1/15 6:05 AM
	}

}
