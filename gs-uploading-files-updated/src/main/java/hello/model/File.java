package hello.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * File class is the object representation of the file table in the database.
 * 
 * @author Lijo Daniel
 */
@Entity
@Table(name = "files")
public class File implements Serializable { // NOPMD by Lijo Daniel on 16/1/15
											// 4:52 AM

	/**
	 * Used for serialization/deserialization.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Handles the file name of the file.
	 */
	@Id
	@Column(name = "file_name")
	private String fileName;

	/**
	 * Handles the bytes of the file.
	 */
	@Lob
	@Basic(fetch=FetchType.LAZY)
	private byte[] content;

	/**
	 * Sets the file name and content to null for this file.
	 */
	public File() {
		//This is an intentional empty constructor
	}

	/**
	 * Sets the file name and content for this file.
	 * 
	 * @param fileName
	 * @param content
	 */
	public File(final String fileName, final byte... content) {
		this.fileName = fileName;
		this.content = content.clone();
	}

	/**
	 * Returns the file name of this file.
	 * 
	 * @return
	 */
	public final String getFileName() {
		return this.fileName;
	}

	/**
	 * Sets the file name for this file.
	 * 
	 * @param fileName
	 */
	public final void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Returns the content of this file.
	 * 
	 * @return
	 */
	public final byte[] getContent() {
		return this.content.clone();
	}

	/**
	 * Sets the content for this file.
	 * 
	 * @param content
	 */
	public final void setContent(final byte... content) {
		this.content = content.clone();
	}

}