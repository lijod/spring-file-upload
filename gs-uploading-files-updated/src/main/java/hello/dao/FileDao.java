package hello.dao;

import java.io.InputStream;
import java.sql.SQLException;

/**
 * This interface handles data access layer.
 * 
 * @author Lijo Daniel
 */
public interface FileDao {

	/**
	 * This method persists the file content and the file name in the database.
	 * 
	 * @param fileName
	 * @param content
	 * @throws SQLException 
	 */
	void persistFile(String fileName, InputStream content) throws SQLException;

}
