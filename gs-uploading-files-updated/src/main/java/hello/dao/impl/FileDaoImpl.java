package hello.dao.impl;

import hello.constant.FileConstant;
import hello.dao.FileDao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.commons.io.IOUtils;

/**
 * An implementation of FileDao to access data from database using JPA.
 * 
 * @author Lijo Daniel
 */
public final class FileDaoImpl implements FileDao {

	/**
	 * an instance of entity manager factory to get the entity manager.
	 */
	final private static EntityManagerFactory EMF = Persistence
			.createEntityManagerFactory(FileConstant.PERSISTENCE_UNIT_NAME);
	/**
	 * the instance of entity manager to manipulate data.
	 */
	final private static EntityManager ENTITY_MANAGER = EMF
			.createEntityManager();
	/**
	 * the instance of this class.
	 */
	final private static FileDaoImpl INSTANCE = new FileDaoImpl();

	private FileDaoImpl() {
	}

	/**
	 * Returns the instance of FileDaoImpl class.
	 * 
	 * @return
	 */
	public static FileDaoImpl getInstance() {
		return INSTANCE;
	}

	/**
	 * Persists data to database.
	 * 
	 * @param fileName
	 * @param content
	 * @throws SQLException 
	 */
	@Override
	@SuppressWarnings({"PMD.LawOfDemeter", "PMD.DataflowAnomalyAnalysis"})
	public void persistFile(final String fileName, final InputStream content) throws SQLException {
		PreparedStatement pStatement = null;
		try {
			ENTITY_MANAGER.getTransaction().begin(); 
			// Eclipselink will handle this connection instance
			final Connection connection = ENTITY_MANAGER.unwrap(Connection.class); //NOPMD
			pStatement = connection.prepareStatement("insert into files (file_name, content)"
																		+ " values (?,?)");
			
			pStatement.setString(1, fileName);
			pStatement.setBinaryStream(2, content);
			pStatement.executeUpdate();
			pStatement.close();
			ENTITY_MANAGER.getTransaction().commit();
		} finally {
			if (pStatement != null){
				pStatement.close();
			}
			IOUtils.closeQuietly(content);
		}	
	}

}
